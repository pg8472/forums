<?php
  session_start();

  include "connection/connection.php";
  include "connection/connection_booklet.php";
  include "forums_check_login.php";
  
  $main_topic_id = $_GET["main_topic_id"];
  $sub_topic_id = $_GET["sub_topic_id"];

  //echo "Main=" . $main_topic_id . "/Sub=" .  $sub_topic_id;

  include "update_last_seen.php";

  $queryMainTopicName = "SELECT `main_topic` FROM `main_topics` WHERE `id`=$main_topic_id";
  $resultMainTopicName = mysqli_query($link, $queryMainTopicName);
  $rowMainTopicName = mysqli_fetch_array($resultMainTopicName);
  $main_topic_name = $rowMainTopicName["main_topic"];

  $querySubTopicName = "SELECT `sub_topic` FROM `sub_topics` WHERE `id`=$sub_topic_id";
  $resultSubTopicName = mysqli_query($link, $querySubTopicName);
  $rowSubTopicName = mysqli_fetch_array($resultSubTopicName);
  $sub_topic_name = $rowSubTopicName["sub_topic"];

  $queryAdmin = "SELECT `admin_only` FROM `main_topics` WHERE `id`=$main_topic_id";
  $resultAdmin = mysqli_query($link, $queryAdmin);
  $rowAdmin = mysqli_fetch_array($resultAdmin);
  $isAdmin = $rowAdmin["admin_only"];

  $topic_count = 1;
?>

<!DOCTYPE html>
<html>

<head>

<?php include "../globalcss.php"; ?>
<title>FPSNZ Booklet - Evaluator Main</title>

<link rel="stylesheet" type="text/css" href="forums_css/topic.css?v1.13">

</head>

<body>

<?php

$header = '<div id="forums_main_header">';
$header .= '<h2><u>FPSNZ Evaluator Forums</u></h2>';
$header .= '<h3>'.$sub_topic_name.'</h2>';
$header .= '</div>';

echo $header;

$text = '<div id="navigation">';
$text .= 'Navigation: <a href="forums_main_topic">Main Forums</a> -> ';
$text .= '<a href="forums_sub_topic?main_topic_id='.$main_topic_id.'">'.$main_topic_name.'</a> -> '.$sub_topic_name.'';
$text .= '</div>';

echo $text;

$query = "SELECT * FROM `posts` WHERE `main_topic_id`=$main_topic_id AND `sub_topic_id`=$sub_topic_id AND `deleted`=0 ORDER BY `topic_num`";
$result = mysqli_query($link, $query);

while($row = mysqli_fetch_array($result)){
  test($row, $eval_id, $main_topic_id, $sub_topic_id);

  $GLOBALS["topic_count"] += 1;
}

function test($row, $eval_id, $main_topic_id, $sub_topic_id){
  $date = $row["date_time"];
  $date = strtotime($date);

  $time = date("g:i A", $date);

  $year = date("y", $date);
  $month = date("m", $date);
  $day = date("d", $date);

  $new_date = $day . "/" . $month . "/" . $year . " ("  . $time . ")";

  $edit_id = $row["id"];

  $text = '
  <div class="forum_post">';

  // $text .= '<td>'.CheckLastSeen($main_topic_id, $sub_topic_id, $link, $eval_id).'<strong><u>
  // <a href="topic.php?main_topic_id='.$main_topic_id.'&sub_topic_id='.$sub_topic_id.'">'.$sub_topic_text.'</a></u></strong></td>';

  if($eval_id == $row["eval_id"]){
    $text .= '<div class="heading_orange">';
    $text .= '<div class="post_name_header"><strong><p>#'.$GLOBALS["topic_count"].': Me ('.$row["eval_name"].') - '.$new_date.'</p></strong></div>';
    $text .= '<div class="edit_post_container">

    <a href="edit_post.php?main_topic_id='.$main_topic_id.'&sub_topic_id='.$sub_topic_id.'&edit_id='.$edit_id.'"
    class="btn btn-sm btn-warning">Edit</a>

    <a href="delete_post.php?main_topic_id='.$main_topic_id.'&sub_topic_id='.$sub_topic_id.'&delete_id='.$edit_id.'"
    class="btn btn-sm btn-danger"">Delete</a>

    </div>';
    $text .= '</div>';
  }
  else{
    $text .= '<div class="heading_blue">';
    $text .= '<strong><p>#'.$GLOBALS["topic_count"].': '.$row["eval_name"].' - '.$new_date.'</p></strong>';
    $text .= '</div>';
  }

  $is_edited = $row["is_edited"];
  $edited_date = $row["edited_date"];
  $edited_note = $row["edited_notes"];
  
  $text .= '<div class="show_topic_text">';

  if($is_edited == 1){
    $text .= '* Edited - ' . $edited_date . ' (<a href="#" data-toggle="tooltip" data-html="true"
    data-placement="bottom" title="'.$edited_note.'">View Edit Notes</a>) *<br><br>';
    $text .= '';
  }

  $text .= nl2br($row["text"]);
  $text .= '</div>';



  //$text .= '<textarea class="show_topic_text" readonly>'.$row["text"].'</textarea>';
  $text .= '</div>';

  echo $text;
}


if($isAdmin == 1){
  if($evalcode == "E23" || $evalcode == "E9" || $evalcode == "E8" || $evalcode == "E0"){
    NewPost($main_topic_id,$sub_topic_id,$eval_id,$link);
  }
}
else{
  NewPost($main_topic_id,$sub_topic_id,$eval_id,$link);
}

function NewPost($main_topic_id,$sub_topic_id,$eval_id,$link){
  echo '<div class="new_post">
  <h5><strong>Enter your reply below:</strong></h5>
  <form method="POST" action="post_reply.php">';

  $queryTopicNum = "SELECT `temp_message` FROM `check_latest` WHERE `main_topic_id`=$main_topic_id 
  AND `sub_topic_id`=$sub_topic_id AND `eval_id`=$eval_id";
  $resultTopicNum = mysqli_query($link, $queryTopicNum);
  $rowTopicNum = mysqli_fetch_array($resultTopicNum);
  $temp_message = $rowTopicNum["temp_message"];

  echo '<textarea 
  id="post_reply_class" 
  name="post_reply_text" 
  placeholder="Enter reply here..." 
  cols=80 
  rows=7 
  data-maintopicid="'.$main_topic_id.'" 
  data-subtopicid="'.$sub_topic_id.'" 
  data-evalid="'.$eval_id.'">'.$temp_message.'</textarea>';

  echo '<p></p>
  <input type="submit" name="submit" value="Post Reply" class="btn btn-success">
  <input type="hidden" name="main_topic_id" value="'.$main_topic_id.'">
  <input type="hidden" name="sub_topic_id" value="'.$sub_topic_id.'">
  </form>
  <p></p>
  </div>';
}


?>

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript" src="forums_js/topic.js?v1.03"></script>

</body>

</html>