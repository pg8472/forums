-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2021 at 01:38 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fpsnz_forums`
--

-- --------------------------------------------------------

--
-- Table structure for table `check_latest`
--

CREATE TABLE `check_latest` (
  `id` int(11) NOT NULL,
  `main_topic_name` varchar(500) NOT NULL,
  `sub_topic_name` varchar(500) NOT NULL,
  `main_topic_id` int(11) NOT NULL,
  `sub_topic_id` int(11) NOT NULL,
  `latest_num_seen` int(11) NOT NULL,
  `eval_name` varchar(500) NOT NULL,
  `eval_code` varchar(255) NOT NULL,
  `eval_id` int(11) NOT NULL,
  `problem` varchar(20) NOT NULL,
  `year` varchar(4) NOT NULL,
  `temp_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_topics`
--

CREATE TABLE `main_topics` (
  `id` int(11) NOT NULL,
  `main_topic` text NOT NULL,
  `admin_only` tinyint(1) NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `main_topic_name` varchar(500) NOT NULL,
  `sub_topic_name` varchar(500) NOT NULL,
  `main_topic_id` int(11) NOT NULL,
  `sub_topic_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `topic_num` int(11) NOT NULL,
  `eval_name` varchar(500) NOT NULL,
  `eval_code` varchar(255) NOT NULL,
  `eval_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `problem` varchar(20) NOT NULL,
  `year` varchar(4) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_edited` tinyint(1) NOT NULL,
  `edited_date` datetime NOT NULL,
  `edited_notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_topics`
--

CREATE TABLE `sub_topics` (
  `id` int(11) NOT NULL,
  `sub_topic` varchar(50) NOT NULL,
  `main_topic_id` int(11) NOT NULL,
  `main_topic_name` varchar(500) NOT NULL,
  `date_time` datetime NOT NULL,
  `eval_name` varchar(500) NOT NULL,
  `eval_code` varchar(255) NOT NULL,
  `eval_id` int(11) NOT NULL,
  `problem` varchar(255) NOT NULL,
  `year` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `check_latest`
--
ALTER TABLE `check_latest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_topics`
--
ALTER TABLE `main_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_topics`
--
ALTER TABLE `sub_topics`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `check_latest`
--
ALTER TABLE `check_latest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_topics`
--
ALTER TABLE `main_topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_topics`
--
ALTER TABLE `sub_topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
