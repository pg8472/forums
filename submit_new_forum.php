<?php

session_start();

include "connection/connection.php";
include "connection/connection_booklet.php";
include "forums_check_login.php";

date_default_timezone_set('Pacific/Auckland');

$topic_title_text = mysqli_real_escape_string($link, $_POST["forum_title_text"]);
$date_time = date('Y-m-d H:i:s');
$admin = 0;

if(isset($_POST["admin_only"])){
    $admin = 1;
}

$queryMainTopicName = "INSERT INTO `main_topics` (`main_topic`,`admin_only`,`date_time`) 
VALUES ('$topic_title_text', $admin, '$date_time')";
mysqli_query($link, $queryMainTopicName);

header("location: forums_main_topic.php");

?>