<?php

$locationRoot = "/";
//$locationroot = "/fpsnzbooklet/";

$name = "";
$evalcode = "";
$problem = "";
$year = "";

if(isset($_SESSION['eval_id_session'])){
  $eval_id = $_SESSION['eval_id_session'];
  GetEvalInfo($eval_id, $link_booklet);
}
else if(isset($_COOKIE["eval_id_cookie"])){
  $eval_id = $_COOKIE['eval_id_cookie'];
  GetEvalInfo($eval_id, $link_booklet);
}
else{
  header("location: forums_error");
}

function GetEvalInfo($eval_id, $link_booklet){
  $query = "SELECT * FROM `evaluators` WHERE `id`=$eval_id LIMIT 1";
  $result = mysqli_query($link_booklet, $query);
  $row = mysqli_fetch_array($result);

  $GLOBALS["name"] = mysqli_real_escape_string($link_booklet, $row["christianname"]) . " " . mysqli_real_escape_string($link_booklet, $row["surname"]);
  $GLOBALS["evalcode"] = $row["evaluatorcode"];
  $GLOBALS["problem"] = $row["problem"];
  $GLOBALS["year"] = $row["year"];
}

?>