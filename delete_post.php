<?php
  session_start();

  include "connection/connection.php";
  include "connection/connection_booklet.php";
  include "forums_check_login.php";

  $main_topic_id = $_GET["main_topic_id"];
  $sub_topic_id = $_GET["sub_topic_id"];
  $delete_id = $_GET["delete_id"];

  $queryMainTopicName = "SELECT `main_topic` FROM `main_topics` WHERE `id`=$main_topic_id";
  $resultMainTopicName = mysqli_query($link, $queryMainTopicName);
  $rowMainTopicName = mysqli_fetch_array($resultMainTopicName);
  $main_topic_name = $rowMainTopicName["main_topic"];

  $querySubTopicName = "SELECT `sub_topic` FROM `sub_topics` WHERE `id`=$sub_topic_id";
  $resultSubTopicName = mysqli_query($link, $querySubTopicName);
  $rowSubTopicName = mysqli_fetch_array($resultSubTopicName);
  $sub_topic_name = $rowSubTopicName["sub_topic"];

  $queryPostText = "SELECT `text` FROM `posts` WHERE `id`=$delete_id";
  $resultPostText= mysqli_query($link, $queryPostText);
  $rowPostText = mysqli_fetch_array($resultPostText);
  $post_text = $rowPostText["text"];
?>

<!DOCTYPE html>
<html>

<head>

<?php include "../globalcss.php"; ?>
<title>FPSNZ Booklet - Evaluator Main</title>

<link rel="stylesheet" type="text/css" href="forums_css/topic.css?v1.13">
<link rel="stylesheet" type="text/css" href="forums_css/delete_post.css?v1.02">

</head>

<body>

<?php

$header = '<div id="forums_main_header">';
$header .= '<h2><u>FPSNZ Evaluator Forums</u></h2>';
$header .= '<h3>Delete Post</h2>';
$header .= '</div>';

echo $header;

$text = '<div id="navigation">';
$text .= 'Navigation: <a href="forums_main_topic">Main Forums</a> -> ';
$text .= '<a href="forums_sub_topic?main_topic_id='.$main_topic_id.'">'.$main_topic_name.'</a> -> ';
$text .= '<a href="topic?main_topic_id='.$main_topic_id.'&sub_topic_id='.$sub_topic_id.'">'.$sub_topic_name.'</a> -> Delete Post';
$text .= '</div>';

echo $text;

NewPost($main_topic_id,$sub_topic_id,$delete_id,$eval_id,$link,$post_text);

function NewPost($main_topic_id,$sub_topic_id,$delete_id,$eval_id,$link,$post_text){
  echo '<div class="delete_post_div">
  <h5><strong>Delete Post:</strong></h5>
  <p>If you would like to delete this post click on the "Delete Post" button below.</p>
  <p style="color:red;"><strong><em>Please Note: If you delete this post we might not be able to recover it at a later date.</em></strong></p>
  <form method="POST" action="post_delete.php">';

  $text = '<div class="delete_text_show">';
  $text .= nl2br($post_text);
  $text .= '</div>';

  echo $text;

  echo '
  <p></p>
  <input type="submit" name="submit" value="Delete Post" class="btn btn-danger">
  <input type="hidden" name="main_topic_id" value="'.$main_topic_id.'">
  <input type="hidden" name="sub_topic_id" value="'.$sub_topic_id.'">
  <input type="hidden" name="delete_id" value="'.$delete_id.'">
  </form>
  <p></p>
  </div>';
}


?>

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>

<!-- <script type="text/javascript" src="forums_js/topic.js?v1.02"></script> -->

</body>

</html>