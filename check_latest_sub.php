<?php

function CheckLastSeen($main_topic_id, $sub_topic_id, $link, $eval_id){
  $queryCheckLatestExists = "SELECT `latest_num_seen` FROM `check_latest` WHERE `main_topic_id`=$main_topic_id AND `sub_topic_id`=$sub_topic_id
  AND `eval_id`=$eval_id";
  $resultsLatestExists = mysqli_query($link, $queryCheckLatestExists);
  $rowLatestExists = mysqli_fetch_array($resultsLatestExists);
  $latest_topic_num_seen = $rowLatestExists["latest_num_seen"];

  $queryTopicNum = "SELECT `topic_num` FROM `posts` WHERE `main_topic_id`=$main_topic_id AND `sub_topic_id`=$sub_topic_id ORDER BY `topic_num` DESC";
  $resultTopicNum = mysqli_query($link, $queryTopicNum);
  $rowTopicNum = mysqli_fetch_array($resultTopicNum);
  $latest_topic_num = $rowTopicNum["topic_num"];
  
  if($latest_topic_num_seen < $latest_topic_num){
    return "<span class='explanation_mark'><strong>!</strong> </span>";
  }
    
  return "";

}
?>