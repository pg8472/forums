$(document).ready(function () {
  $("#create_new_topic_btn").on("click", function(){
    $("#new_topic_container").css("display", "table");
  });

  $("#close_button").on("click", function(){
    $("#new_topic_container").css("display", "none");
  });

  $("#topic_title_text_id").on("input propertychange", function(){
    var length = $(this).val().length;

    $("#topic_title_length_id").text(length);
  });

});