$(document).ready(function () {

  $('[data-toggle="tooltip"]').tooltip();
  
  $("#post_reply_class").on("input propertychange", function(){

    var text = $(this).val();
    var main_topic_id = $(this).data("maintopicid");
    var sub_topic_id = $(this).data("subtopicid");
    var eval_id = $(this).data("evalid");

    $.ajax({
      method: "POST",
      url: "connection/temp_reply_update.php",
      data: {text:text, main_topic_id:main_topic_id, sub_topic_id:sub_topic_id, eval_id:eval_id},
      success: function(data){
        console.log(data);
      },
      error: function(){

      }
    });

    console.log(main_topic_id + "/" + sub_topic_id + "/" + eval_id);
  });

});