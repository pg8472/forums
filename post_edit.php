<?php

session_start();

include "connection/connection.php";
include "connection/connection_booklet.php";
include "forums_check_login.php";

date_default_timezone_set('Pacific/Auckland');

$post_reply_text = mysqli_real_escape_string($link, $_POST["post_reply_text"]);
$edit_notes = mysqli_real_escape_string($link, $_POST["edit_notes"]);
$main_topic_id = $_POST["main_topic_id"];
$sub_topic_id = $_POST["sub_topic_id"];
$edit_id = $_POST["edit_id"];
$new_notes = "";

$date_time = date('Y-m-d H:i:s');

$queryNotes = "SELECT `edited_notes` FROM `posts` WHERE `id`=$edit_id";
$resultNotes = mysqli_query($link, $queryNotes);
$rowNotes = mysqli_fetch_array($resultNotes);

if($rowNotes["edited_notes"] == "" && $edit_notes != ""){
    $new_notes = "- " . $edit_notes;
}
else if($edit_notes != ""){
    $new_notes = mysqli_real_escape_string($link, $rowNotes["edited_notes"]) . "\r\n\r\n- " . $edit_notes;
}
else{
    $new_notes = mysqli_real_escape_string($link, $rowNotes["edited_notes"]);
}

$query = "UPDATE `posts` SET `text`='$post_reply_text',`is_edited`=1,`edited_date`='$date_time',
`edited_notes`='$new_notes' WHERE `id`=$edit_id";
mysqli_query($link, $query);

header("location: topic.php?main_topic_id=" . $main_topic_id . "&sub_topic_id=" . $sub_topic_id);

?>