<?php
  session_start();

  include "connection/connection.php";
  include "connection/connection_booklet.php";
  include "forums_check_login.php";

  $main_topic_id = $_GET["main_topic_id"];
  $sub_topic_id = $_GET["sub_topic_id"];
  $edit_id = $_GET["edit_id"];

  $queryMainTopicName = "SELECT `main_topic` FROM `main_topics` WHERE `id`=$main_topic_id";
  $resultMainTopicName = mysqli_query($link, $queryMainTopicName);
  $rowMainTopicName = mysqli_fetch_array($resultMainTopicName);
  $main_topic_name = $rowMainTopicName["main_topic"];

  $querySubTopicName = "SELECT `sub_topic` FROM `sub_topics` WHERE `id`=$sub_topic_id";
  $resultSubTopicName = mysqli_query($link, $querySubTopicName);
  $rowSubTopicName = mysqli_fetch_array($resultSubTopicName);
  $sub_topic_name = $rowSubTopicName["sub_topic"];

  $queryPostText = "SELECT `text` FROM `posts` WHERE `id`=$edit_id";
  $resultPostText= mysqli_query($link, $queryPostText);
  $rowPostText = mysqli_fetch_array($resultPostText);
  $post_text = $rowPostText["text"];
?>

<!DOCTYPE html>
<html>

<head>

<?php include "../globalcss.php"; ?>
<title>FPSNZ Booklet - Evaluator Main</title>

<link rel="stylesheet" type="text/css" href="forums_css/topic.css?v1.13">

</head>

<body>

<?php

$header = '<div id="forums_main_header">';
$header .= '<h2><u>FPSNZ Evaluator Forums</u></h2>';
$header .= '<h3>Edit Post</h2>';
$header .= '</div>';

echo $header;

$text = '<div id="navigation">';
$text .= 'Navigation: <a href="forums_main_topic">Main Forums</a> -> ';
$text .= '<a href="forums_sub_topic?main_topic_id='.$main_topic_id.'">'.$main_topic_name.'</a> -> ';
$text .= '<a href="topic?main_topic_id='.$main_topic_id.'&sub_topic_id='.$sub_topic_id.'">'.$sub_topic_name.'</a> -> Edit Post';
$text .= '</div>';

echo $text;

NewPost($main_topic_id,$sub_topic_id,$edit_id,$eval_id,$link,$post_text);

function NewPost($main_topic_id,$sub_topic_id,$edit_id,$eval_id,$link,$post_text){
  echo '<div class="new_post">
  <h5><strong>Edit Post:</strong></h5>
  <p>If you would like to make any edits to your post, please do so below as necessary.
  Once you have finished, click on the "Submit Edited Post" button below.</p>
  <form method="POST" action="post_edit.php">';

  $queryTopicNum = "SELECT `temp_message` FROM `check_latest` WHERE `main_topic_id`=$main_topic_id 
  AND `sub_topic_id`=$sub_topic_id AND `eval_id`=$eval_id";
  $resultTopicNum = mysqli_query($link, $queryTopicNum);
  $rowTopicNum = mysqli_fetch_array($resultTopicNum);
  $temp_message = $rowTopicNum["temp_message"];

  echo '<textarea 
  id="post_reply_class" 
  name="post_reply_text" 
  placeholder="Enter reply here..." 
  cols=80 
  rows=7>'.$post_text.'</textarea>';

  echo '<h6>Optional:</h6>
  <p>If you would like to edit notes to let other evaulators what you have changed,
  please enter it in the box below (e.g. Fixing grammar).
  This is not required and you can just leave the box blank if you do not wish to enter any.</p>
  </p>

  <p>Edit Notes:</p>

  <textarea 
  placeholder="Optional edit notes here..." 
  cols=40 
  rows=3 
  name="edit_notes"></textarea>
  
  
  <p></p>
  <input type="submit" name="submit" value="Submit Editied Post" class="btn btn-success">
  <input type="hidden" name="main_topic_id" value="'.$main_topic_id.'">
  <input type="hidden" name="sub_topic_id" value="'.$sub_topic_id.'">
  <input type="hidden" name="edit_id" value="'.$edit_id.'">
  </form>
  <p></p>
  </div>';
}


?>

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>

<!-- <script type="text/javascript" src="forums_js/topic.js?v1.02"></script> -->

</body>

</html>