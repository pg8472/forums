<?php
  session_start();

  include "connection/connection.php";
  include "connection/connection_booklet.php";
  include "forums_check_login.php";
  include "setup_latest_main.php";
  include "check_latest_main.php";
?>

<!DOCTYPE html>
<html>

<head>

<?php include "../globalcss.php"; ?>
<title>FPSNZ Booklet - Evaluator Main</title>

<link rel="stylesheet" type="text/css" href="forums_css/forums_main_topic.css?v1.09">

</head>

<body>

<?php
  $filename = basename(__FILE__);
  //include "evaluatornavbar.php";
?>

<div id="forums_main_header">
  <h2><u>FPSNZ Evaluator Forums</u></h2>
  <h3>Main Forums</h2>
</div>

<?php

$text = '<div id="navigation">';
$text .= 'Navigation: <a href="forums_main_topic">Main Forums</a>';
$text .= '</div>';

echo $text;

function CreateNewForum($evalcode){

  if($evalcode == "E23" || $evalcode == "E9" || $evalcode == "E8" || $evalcode == "E0"){
    $create_new_topic_btn = '<div id="new_topic_container_btn">';
    $create_new_topic_btn .= '<button class="btn btn-success" id="create_new_topic_btn">Create New Forum</button>';
    $create_new_topic_btn .= '</div>';

    echo $create_new_topic_btn;
  }

  $create_new_topic = '<div id="new_topic_container">';
  $create_new_topic .= '<div id="close_button">X</div>
  <h4><u>Create New Forum</u></h4>
  <p>Please enter a title for the forum below. When you are done click on the "Submit New Forum" button below.</p>
  <form method="POST" action="submit_new_forum.php">
  <p>Forum Title: <input id="topic_title_text_id" type="text" size=70 name="forum_title_text" maxlength="50">
  <strong><span id="topic_title_length_id">0</span>/50</strong></p>
  <p>Admin Only: <input type="checkbox" name="admin_only"></p>
  <input type="submit" value="Submit New Forum" class="btn btn-success">
  </form>
  ';
  $create_new_topic .= '</div>';
  
  echo $create_new_topic;
}

CreateNewForum($evalcode);

$booklettable = '<div id="forum_container">';

$booklettable .= '<table class="table table-striped" id="adminschoolstable" style="width: 800px">';
$booklettable .= '<thead class="thead-dark">';
$booklettable .= '<tr>';
$booklettable .= '<th>Forum</th>';
$booklettable .= '<th>Topics</th>';
$booklettable .= '<th>Total Posts</th>';
$booklettable .= '</tr>';
$booklettable .= '</thead>';
$booklettable .= '<tbody>';

$booklettable .= Topic($link, $eval_id);

$booklettable .= '</tbody>';
$booklettable .= '</table></div>';

echo $booklettable;

function Topic($link, $eval_id){

  $query = "SELECT * FROM `main_topics`";
  $result = mysqli_query($link, $query);

  $text = "";

  while($row = mysqli_fetch_array($result)){

    $forum_id = $row["id"];
    $main_topic_text = $row["main_topic"];

    $queryCountSubtopics = "SELECT * FROM `sub_topics` WHERE `main_topic_id`=$forum_id";
    $resultCountSubtopics = mysqli_query($link, $queryCountSubtopics);
    $SubtopicCount = mysqli_num_rows($resultCountSubtopics);

    $text .= '<tr>';
    $text .= '<td>'.CheckLastSeen($forum_id, $link, $eval_id).'<strong><u><a href="forums_sub_topic.php?main_topic_id='.$forum_id.'">'.$main_topic_text.'</a></u></strong></td>';
    $text .= '<td>'.$SubtopicCount.'</td>';
    $text .= '<td>'.GetNumOfPosts($forum_id, $link).'</td>';
    $text .= '</tr>';
  }
  
  return $text;
}

function GetNumOfPosts($forum_id, $link){
  $query = "SELECT * FROM `posts` WHERE `main_topic_id`=$forum_id";
  $result = mysqli_query($link, $query);
  $count = mysqli_num_rows($result);

  return $count;
}

?>

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript" src="forums_js/forums_main_topic.js?v1.01"></script>

</body>

</html>
