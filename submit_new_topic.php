<?php

session_start();

include "connection/connection.php";
include "connection/connection_booklet.php";
include "forums_check_login.php";
//include "new_last_seen.php";

date_default_timezone_set('Pacific/Auckland');

$topic_title_text = mysqli_real_escape_string($link, $_POST["topic_title_text"]);
$initial_post_text = mysqli_real_escape_string($link, $_POST["initial_post_text"]);
$main_topic_id = $_POST["main_topic_id"];

$queryMainTopicName = "SELECT `main_topic` FROM `main_topics` WHERE `id`=$main_topic_id";
$resultMainTopicName = mysqli_query($link, $queryMainTopicName);
$rowMainTopicName = mysqli_fetch_array($resultMainTopicName);
$main_topic_name = $rowMainTopicName["main_topic"];

$date_time = date('Y-m-d H:i:s');

$query = "INSERT INTO `sub_topics` (`sub_topic`,`main_topic_id`,`main_topic_name`,`date_time`,`eval_name`,
`eval_code`,`eval_id`,`problem`,`year`) 
VALUES ('$topic_title_text',$main_topic_id,'$main_topic_name','$date_time','$name',
'$evalcode',$eval_id,'$problem','$year')";
mysqli_query($link, $query);

$latest_id = mysqli_insert_id($link);

if($initial_post_text != ""){
    $query = "INSERT INTO `posts` (`main_topic_name`,`sub_topic_name`,`main_topic_id`,`sub_topic_id`,`text`,
    `topic_num`,`date_time`,`eval_code`,`eval_name`,`eval_id`,`problem`,`year`) 
    VALUES ('$main_topic_name','$topic_title_text',$main_topic_id,$latest_id,'$initial_post_text',
    1,'$date_time','$evalcode','$name',$eval_id,'$problem','$year')";
    mysqli_query($link, $query);

    $queryInsertLatest = "INSERT INTO `check_latest` (`main_topic_id`,`sub_topic_id`,`latest_num_seen`,
    `eval_name`,`eval_code`,`eval_id`,`problem`,`year`) VALUES ($main_topic_id, $latest_id, 1, '$name',
    '$evalcode', $eval_id, '$problem', '$year')";
    mysqli_query($link, $queryInsertLatest);

    header("location: topic.php?main_topic_id=" . $main_topic_id . "&sub_topic_id=" . $latest_id);
}
else{
    $queryInsertLatest = "INSERT INTO `check_latest` (`main_topic_id`,`sub_topic_id`,`latest_num_seen`,
    `eval_name`,`eval_code`,`eval_id`,`problem`,`year`) VALUES ($main_topic_id, $latest_id, 0, '$name',
    '$evalcode', $eval_id, '$problem', '$year')";
    mysqli_query($link, $queryInsertLatest);

    header("location: forums_sub_topic.php?main_topic_id=" . $main_topic_id);
}

?>