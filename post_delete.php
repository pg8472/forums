<?php

session_start();

include "connection/connection.php";
include "connection/connection_booklet.php";
include "forums_check_login.php";

date_default_timezone_set('Pacific/Auckland');

$main_topic_id = $_POST["main_topic_id"];
$sub_topic_id = $_POST["sub_topic_id"];
$delete_id = $_POST["delete_id"];

$query = "UPDATE `posts` SET `deleted`=1 WHERE `id`=$delete_id";
mysqli_query($link, $query);

header("location: topic.php?main_topic_id=" . $main_topic_id . "&sub_topic_id=" . $sub_topic_id);

?>