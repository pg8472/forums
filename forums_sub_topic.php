<?php
  session_start();

  include "connection/connection.php";
  include "connection/connection_booklet.php";
  include "forums_check_login.php";
  include "check_latest_sub.php";
  
  $main_topic_id = $_GET["main_topic_id"];

  $queryMainTopicName = "SELECT `main_topic` FROM `main_topics` WHERE `id`=$main_topic_id";
  $resultMainTopicName = mysqli_query($link, $queryMainTopicName);
  $rowMainTopicName = mysqli_fetch_array($resultMainTopicName);
  $main_topic_name = $rowMainTopicName["main_topic"];
?>

<!DOCTYPE html>
<html>

<head>

<?php include "../globalcss.php"; ?>
<title>FPSNZ Booklet - Evaluator Main</title>

<link rel="stylesheet" type="text/css" href="forums_css/forums_sub_topic.css?v1.08">

</head>

<body>

<?php

$header = '<div id="forums_main_header">';
$header .= '<h2><u>FPSNZ Evaluator Forums</u></h2>';
$header .= '<h3>'.$main_topic_name.'</h2>';
$header .= '</div>';

echo $header;

$text = '<div id="navigation">';
$text .= 'Navigation: <a href="forums_main_topic">Main Forums</a> -> '.$main_topic_name.'';
$text .= '</div>';

echo $text;

$queryAdmin = "SELECT `admin_only` FROM `main_topics` WHERE `id`=$main_topic_id";
$resultAdmin = mysqli_query($link, $queryAdmin);
$rowAdmin = mysqli_fetch_array($resultAdmin);
$isAdmin = $rowAdmin["admin_only"];

if($isAdmin == 1){
  if($evalcode == "E23" || $evalcode == "E9" || $evalcode == "E8" || $evalcode == "E0"){
    $create_new_topic_btn = '<div id="new_topic_container_btn">';
    $create_new_topic_btn .= '<button class="btn btn-success" id="create_new_topic_btn">Create New Topic</button>';
    $create_new_topic_btn .= '</div>';
    
    echo $create_new_topic_btn;
  }
}
else{
  $create_new_topic_btn = '<div id="new_topic_container_btn">';
  $create_new_topic_btn .= '<button class="btn btn-success" id="create_new_topic_btn">Create New Topic</button>';
  $create_new_topic_btn .= '</div>';
  
  echo $create_new_topic_btn;
}

$create_new_topic = '<div id="new_topic_container">';
$create_new_topic .= '<div id="close_button">X</div>
<h4><u>Create New Topic</u></h4>
<p>Please enter a title for your new topic below, and your initial post if you wish. If you don\'t want
to enter an initial post yet, just leave the "Initial Post" box blank. When you are done click on the "Submit New Topic" button below.</p>
<form method="POST" action="submit_new_topic.php">
<input type="hidden" name="main_topic_id" value="'.$main_topic_id.'">
<p>Topic Title: <input id="topic_title_text_id" type="text" size=70 name="topic_title_text" maxlength="50">
<strong><span id="topic_title_length_id">0</span>/50</strong></p>
<p>Initial Post:</p>
<textarea cols=100 rows=10 name="initial_post_text"></textarea>
<input type="submit" value="Submit New Topic" class="btn btn-success">
</form>
';
$create_new_topic .= '</div>';

echo $create_new_topic;

$booklettable = '<div id="forum_container">';

$booklettable .= '<table class="table table-striped" id="adminschoolstable" style="width: 800px">';
$booklettable .= '<thead class="thead-dark">';
$booklettable .= '<tr>';
$booklettable .= '<th>Topic</th>';
$booklettable .= '<th>Posts</th>';
$booklettable .= '<th>Topic Creator</th>';
$booklettable .= '<th>Latest Post</th>';
$booklettable .= '</tr>';
$booklettable .= '</thead>';
$booklettable .= '<tbody>';

$booklettable .= Topic($link, $main_topic_id, $eval_id);

$booklettable .= '</tbody>';
$booklettable .= '</table></div>';

echo $booklettable;

function Topic($link, $main_topic_id, $eval_id){

  $query = "SELECT * FROM `sub_topics` WHERE `main_topic_id`=$main_topic_id";
  $result = mysqli_query($link, $query);

  $text = "";

  while($row = mysqli_fetch_array($result)){

    $sub_topic_id = $row["id"];
    $sub_topic_text = $row["sub_topic"];

    $text .= '<tr>';
    $text .= '<td>'.CheckLastSeen($main_topic_id, $sub_topic_id, $link, $eval_id).'<strong><u>
    <a href="topic.php?main_topic_id='.$main_topic_id.'&sub_topic_id='.$sub_topic_id.'">'.$sub_topic_text.'</a></u></strong></td>';
    $text .= '<td>'.GetNumOfPosts($main_topic_id, $sub_topic_id, $link).'</td>';
    $text .= '<td>'.$row["eval_name"].'</td>';
    $text .= '<td>'.CheckLatestPost($main_topic_id, $sub_topic_id, $link).'</td>';
    $text .= '</tr>';
  }

  return $text;
}

function GetNumOfPosts($main_topic_id, $sub_topic_id, $link){
  $query = "SELECT * FROM `posts` WHERE `main_topic_id`=$main_topic_id 
  AND `sub_topic_id`=$sub_topic_id";
  $result = mysqli_query($link, $query);
  $count = mysqli_num_rows($result);

  return $count;
}

function CheckLatestPost($main_topic_id, $sub_topic_id, $link){

  $query = "SELECT * FROM `posts` WHERE `main_topic_id`=$main_topic_id 
  AND `sub_topic_id`=$sub_topic_id ORDER BY `topic_num` DESC";
  $result = mysqli_query($link, $query);
  $row = mysqli_fetch_array($result);

  $date = $row["date_time"];
  $date = strtotime($date);

  $time = date("g:i A", $date);

  $year = date("y", $date);
  $month = date("m", $date);
  $day = date("d", $date);

  $new_date = $day . "/" . $month . "/" . $year . " ("  . $time . ")";

  $text = $row["eval_name"] . " - " . $new_date;

  return $text;
}

?>

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript" src="forums_js/forums_sub_topic.js?v1.03"></script>

</body>

</html>