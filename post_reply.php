<?php

session_start();

include "connection/connection.php";
include "connection/connection_booklet.php";
include "forums_check_login.php";

date_default_timezone_set('Pacific/Auckland');

$post_reply_text = mysqli_real_escape_string($link, $_POST["post_reply_text"]);
$main_topic_id = $_POST["main_topic_id"];
$sub_topic_id = $_POST["sub_topic_id"];

$queryMainTopicName = "SELECT `main_topic` FROM `main_topics` WHERE `id`=$main_topic_id";
$resultMainTopicName = mysqli_query($link, $queryMainTopicName);
$rowMainTopicName = mysqli_fetch_array($resultMainTopicName);
$main_topic_name = $rowMainTopicName["main_topic"];

$querySubTopicName = "SELECT `sub_topic` FROM `sub_topics` WHERE `id`=$sub_topic_id";
$resultSubTopicName = mysqli_query($link, $querySubTopicName);
$rowSubTopicName = mysqli_fetch_array($resultSubTopicName);
$sub_topic_name = $rowSubTopicName["sub_topic"];

$date_time = date('Y-m-d H:i:s');

$queryGetLastNumber = "SELECT `topic_num` FROM `posts` WHERE `main_topic_id`=$main_topic_id
AND `sub_topic_id`=$sub_topic_id AND `problem`='$problem' AND `year`='$year' ORDER BY `topic_num` DESC";
$resultGetLastNumber = mysqli_query($link, $queryGetLastNumber);
$row = mysqli_fetch_array($resultGetLastNumber);

$new_number = (int)$row["topic_num"] + 1;

$query = "INSERT INTO `posts` (`main_topic_name`,`sub_topic_name`,`main_topic_id`,`sub_topic_id`,`text`,
`topic_num`,`date_time`,`eval_code`,`eval_name`,`eval_id`,`problem`,`year`) 
VALUES ('$main_topic_name','$sub_topic_name',$main_topic_id,$sub_topic_id,'$post_reply_text',
$new_number,'$date_time','$evalcode','$name',$eval_id,'$problem','$year')";
mysqli_query($link, $query);

$querySetTempBlank = "UPDATE `check_latest` SET `temp_message`='' WHERE `main_topic_id`=$main_topic_id 
AND `sub_topic_id`=$sub_topic_id AND `eval_id`=$eval_id";
mysqli_query($link, $querySetTempBlank);

header("location: topic.php?main_topic_id=" . $main_topic_id . "&sub_topic_id=" . $sub_topic_id);

?>